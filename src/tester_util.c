#include "tester_util.h"
struct test_list* test_create(bool (*test)()) {
	struct test_list* my_test = malloc(sizeof(struct test_list));
	my_test->test = test;
	my_test->number = 0;
	return my_test;
}

struct test_list* test_last(struct test_list* list) {
	struct test_list* current_test = list;
	if (current_test) {
		while (current_test->next != NULL) {
			current_test = current_test->next;
		}
	}
	return current_test;
}

void add_test(struct test_list** old, bool (*test)()) {
	if (*old == NULL) (*old) = test_create(test);
	else {
		struct test_list* last_test = test_last(*old);
		struct test_list* new_test = test_create(test);
		new_test->number = last_test->number + 1;
		last_test->next = new_test;
	}
}


void complete_all_tests(struct test_list* list) {
	struct test_list* current_test = list;
	bool complete = true;
	while (current_test) {
		bool (*test)() = current_test->test;
		bool current_complete = (*test)();
		if (!current_complete) {
			complete = false;
			printf("The test %d failed, execution of further tests has been stopped\n", current_test->number + 1);
		}
		current_test = current_test->next;
	}
	if (complete) {
		printf("All test PASSED\n");
	}
}
