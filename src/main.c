#include <unistd.h>
#include <stdlib.h>
#include "mem.h"
#include "mem_internals.h"
#include "tester_util.h"

void* heap;

bool test1() {
	printf("Test - STARTED - Normal successful memory allocation.\n");
	void* block0 = _malloc(1000);
	if(block0 == NULL) {
  		printf("FAILED - Memory not allocated\n");
		return false;
	}
  	printf("Test - PASSED!\n");
  	_free(block0);
	return true;
}

bool test2() {
	printf("Test - STARTED - Freeing one block from several allocated ones\n");
	void* block0 = _malloc(50);
	void* block1 = _malloc(50);
  	void* block2 = _malloc(50);
	void* block3;
	if (!block0 || !block1 || !block2) {
		printf("FAILED - incorrect memory allocated\n");
		return false;
	}
	_free(block1);
	block3 = _malloc(30);
	if (!block3) {
		printf("FAILED - incorrect memory allocated\n");
		return false;
	}
	if (block3 != block1) {
		printf("FAILED - incorrect addr of blocks allocated\n");
		return false;
	}
	_free(block0);
	_free(block2);
	_free(block3);
	printf("Test - PASSED!\n");
	return true;
}

bool test3() {
	printf("Test - STARTED - Freeing two blocks from several allocated ones.\n");
	void* block0 = _malloc(100);
	void* block1 = _malloc(100);
	void* block2 = _malloc(100);
	void* block3 = _malloc(100);
	void* block4;
	if (!block0 || !block1 || !block2 || !block3) {
		printf("FAILED - incorrect memory allocated\n");
		return false;
	}
	_free(block1);
	_free(block2);
	block4 = _malloc(150);
	if (!block4) {
		printf("FAILED - incorrect memory allocated\n");
		return false;
	}
	if (block4 != block1) {
		printf("FAILED - incorrect addr of blocks allocated"); 
		return false;
	}
	_free(block0);
	_free(block3);
	_free(block4);
	printf("Test - PASSED!\n");
	return true;
}

bool test4() {
	printf("Test - STARTED - The memory has run out, the new memory region expands the old one.\n");
	void* block0 = _malloc(10100);
	if (!block0) {
		printf("FAILED - incorrect addr of blocks allocated"); 
		return false;
	}
	if (block0 != ((int8_t*) HEAP_START) + offsetof( struct block_header, contents )) {
		printf("FAILED - new memory region has not expanded\n");
		return false;
	}
	printf("Test - PASSED!\n");
	return true;
}

bool test5() {
	printf("Test - STARTED - The memory has run out, the old memory region cannot be expanded due to a different allocated address range, the new region is allocated elsewhere.\n");
	
	mmap((void*)0x1221000, 10000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
	void* block0 = _malloc(1000);
	if (!block0) {
		printf("FAILED - incorrect addr of blocks allocated"); 
		return false;
	}
	if (block0 == (int8_t *) HEAP_START + offsetof(struct block_header, contents)) {
        	printf("FAILED - the new region allocated on old memory region\n");
		return false;
	}
	_free(block0);
	printf("Test - PASSED!\n");
	return true;
}

int main() {
	heap = heap_init(10000);
	struct test_list* tests = NULL;
	add_test(&tests, &test1);
	add_test(&tests, &test2);
	add_test(&tests, &test3);
	add_test(&tests, &test4);
	add_test(&tests, &test5);
	complete_all_tests(tests);
}
