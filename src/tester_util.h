#ifndef _UTIL_TESTER_
#define _UTIL_TESTER_
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
struct test_list {
	bool (*test)();
	struct test_list* next;
	int number;
};

struct test_list* test_last(struct test_list* list);

struct test_list* test_create(bool (*test)());

void add_test(struct test_list** old, bool (*test)());

void complete_all_tests(struct test_list* list);

#endif
